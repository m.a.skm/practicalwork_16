﻿#include <iostream>
using namespace std;

int main()
{
    const int N = 10;
    int Array[N][N] = {};    
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            Array[i][j] = i + j;
            cout << Array[i][j] << "\t";            
        }
        cout << endl;
    }
    const int CurrentCalendarDate = 8;
    const int Remainder = (CurrentCalendarDate % N);
    int Str[N] = {};
    int Sum = 0;
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; j++)
        {            
            Str[i] += Array[Remainder][j];
            Sum = Str[i];
        }         
    }
    cout << Sum << endl;
    return 0;
}


